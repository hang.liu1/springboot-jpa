package com.twuc.webApp.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.domain.ProductLine;
import com.twuc.webApp.domain.ProductLineRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;


import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ProductLineTest extends APITestBase {
    @Autowired
    private ProductLineRepository productLineRepository;

    private ProductLine createProductLine(String line){
        return new ProductLine(line, "description");
    }

    private MockHttpServletRequestBuilder createProductLineRequestBulider(ProductLine productLine) throws JsonProcessingException {
        return post("/api/product-lines")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(
                        new ObjectMapper()
                                .writeValueAsString(productLine));
    }

    @Test
    void should_post_productline() throws Exception {
        getMockMvc().perform(createProductLineRequestBulider(createProductLine("a")))
                .andExpect(status().is(201));

        assertTrue(productLineRepository.findById("a").isPresent());
    }


    @Test
    void should_get_productlines() throws Exception {
        flushAndClear(() -> {
            productLineRepository.save(createProductLine("b"));
            productLineRepository.save(createProductLine("a"));
            productLineRepository.save(createProductLine("c"));

        });

        getMockMvc().perform(get("/api/product-lines"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].productLine").value("a"))
                .andExpect(jsonPath("$[1].productLine").value("b"))
                .andExpect(jsonPath("$[2].productLine").value("c"));


    }
}
