package com.twuc.webApp.web;

import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductLine;
import com.twuc.webApp.domain.ProductLineRepository;
import com.twuc.webApp.domain.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class JpaTest  {
    
    @Autowired
    private ProductRepository productRepository;
    
    @Autowired
    private ProductLineRepository productLineRepository;

    @Autowired
    private EntityManager entityManager;

    private void flushAndClear(Runnable run){
        run.run();
        entityManager.flush();
        entityManager.clear();
    }

    @Test
    void name() {
        flushAndClear(() -> {
            ProductLine productLine = new ProductLine("a", "aa");
            productLineRepository.save(productLine);
        });

        flushAndClear(() -> {
            productLineRepository.findById("a").orElseThrow(RuntimeException::new);
        });

    }
}
