package com.twuc.webApp.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ProductTest extends APITestBase {

    @Autowired
    private ProductRepository productRepository;

    private Product createProductByCode(String code){
        return new Product(code, "productName", null, "ps", "productVendor", "productDescription", (short)0, new BigDecimal(0), new BigDecimal(0)) ;
    }

    private MockHttpServletRequestBuilder createProduct(Product product) throws Exception {
        return post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(
                        new ObjectMapper()
                                .writeValueAsString(product));
    }

    @Test
    void should_post_product() throws Exception {
        getMockMvc().perform(createProduct(createProductByCode("a")))
                .andExpect(status().is(201));

        assertTrue(productRepository.findById("a").isPresent());
    }

    @Test
    void should_get_products() throws Exception {
        flushAndClear(() -> {
            productRepository.save(createProductByCode("b"));
            productRepository.save(createProductByCode("a"));
            productRepository.save(createProductByCode("c"));

        });


        getMockMvc().perform(get("/api/products"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].productCode").value("a"))
                .andExpect(jsonPath("$[1].productCode").value("b"))
                .andExpect(jsonPath("$[2].productCode").value("c"));

    }


}
