package com.twuc.webApp.web;

import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/products")
public class ProductController {

    private ProductRepository productRepository;

    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @GetMapping
    public List<Product> getProducts(){
        return productRepository.findAllByOrderByProductCode();
    }



    @PostMapping
    public ResponseEntity postProduct(@RequestBody Product product){
        productRepository.saveAndFlush(product);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }


}
