package com.twuc.webApp.web;

import com.twuc.webApp.domain.ProductLine;
import com.twuc.webApp.domain.ProductLineRepository;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/product-lines")
public class ProductLineController {
    private ProductLineRepository productLineRepository;

    public ProductLineController(ProductLineRepository productLineRepository) {
        this.productLineRepository = productLineRepository;
    }

    @PostMapping
    public ResponseEntity postProductLine(@RequestBody ProductLine productLine){
        productLineRepository.saveAndFlush(productLine);
        return ResponseEntity.status(201).build();
    }

    @GetMapping
    public List<ProductLine> getProductLines(){
        return productLineRepository.findAllByOrderByProductLine();
    }
}
