package com.twuc.webApp.contract;

import com.twuc.webApp.domain.ProductLine;

import java.math.BigDecimal;

public class ProductRequest {
    private String productCode;

    private String productName;

    private ProductLine productLine;

    private String productScale;

    private String productVendor;

    private String productDescription;

    private Short quantityInStock;

    private BigDecimal buyPrice;

    private BigDecimal MSRP;

    public ProductRequest() {
    }

    public ProductRequest(String productCode, String productName, ProductLine productLine, String productScale, String productVendor, String productDescription, Short quantityInStock, BigDecimal buyPrice, BigDecimal MSRP) {
        this.productCode = productCode;
        this.productName = productName;
        this.productLine = productLine;
        this.productScale = productScale;
        this.productVendor = productVendor;
        this.productDescription = productDescription;
        this.quantityInStock = quantityInStock;
        this.buyPrice = buyPrice;
        this.MSRP = MSRP;
    }

    public String getProductCode() {
        return productCode;
    }

    public String getProductName() {
        return productName;
    }

    public ProductLine getProductLine() {
        return productLine;
    }

    public String getProductScale() {
        return productScale;
    }

    public String getProductVendor() {
        return productVendor;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public Short getQuantityInStock() {
        return quantityInStock;
    }

    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    public BigDecimal getMSRP() {
        return MSRP;
    }
}
